# MSPConvert

## What is MSPConvert?

MSPConvert is a really simple python script to convert old formats of Microsoft Paint to a format that GIMP can handle natively (XBM).  
Because both version 1 and version 2 MSP files are black and white only, each pixel is represented directly by a bit. Converting version 1 is very simple: just copy the data over.  
Version 2 implemented some simple run-length encoding, so a few extra steps are needed.  

## Usage

To convert a file called EXAMPLE.MSP to EXAMPLE_converted.xbm, just run:  
`./mspconvert.py EXAMPLE`  
It should detect the version automatically, and then write the output file.

Then fire up GIMP, open the xbm file, and you can then export it as any other modern format (PNG, JPG, BMP etc.)

## Example

The examples/ directory has an example of an avatar I created for my friend floppydisk. It has the original PNG, the converted XBM, and the exported PNG.

![floppydisk](examples/FLOPPY.png "Floppydisk's Avatar")

Pretty awesome, right?

## License

The code is under Mozilla Public License v2. Feel free to modify and share as you wish -- just make sure your changes are released under the same license!
